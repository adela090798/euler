# Четные числа Фибоначчи
# Каждый следующий элемент ряда Фибоначчи получается при сложении двух предыдущих.
# Начиная с 1 и 2, первые 10 элементов будут:
# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
#
# Найдите сумму всех четных элементов ряда Фибоначчи, которые не превышают четыре миллиона.


numbers = [1, 2]
n = 0
result = []
l = len(numbers)
fib_sum = 1
while n < 4000000:
    n += 1
    for i in range(l):
        if (numbers[-2] + numbers[-1]) < 4000000:
            n = numbers[-2] + numbers[-1]
            numbers.append(n)
            print(n)

for i in numbers:
    if i % 2 == 0:
        result.append(i)
print(result)
for i in result:
    fib_sum *= i
print(fib_sum)
